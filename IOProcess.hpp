/**
	*
	* Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project (https://gitlab.com/desq/)
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* at your option, any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
**/

#pragma once

#include <QString>
#include <QDateTime>

namespace DesQ {

    /** IO Process Type enums */
    enum IOProcessType {
        Copy = 0x5CF670,				// Copy file/dir
        Move,							// Move file/dir
        SymLink,                        // Create symbolic link
        Link,                           // Create hard link - Valid only within the same device
        Trash,							// Trash file/dir
        Restore,						// Restore file/dir
        Delete							// Delete file/dir
    };

    /** IO Process State enums */
    enum IOProcessState {
        NotStarted = 0x7A242A,			// We are yet to begin doing anything with this (for delayed start, etc)
        Starting,						// Listing the sources
        Started,						// Process is on going
        Paused,							// Process is paused
        Canceled,						// Process was cancelled
        Completed						// Process is complete (with/without errors)
    };

    /** Conflict Resolution Strategy */
    enum IOConflictResolutionStrategy {
        ReplaceExisting = 0x498E38,		// Blindly replace the existing target file
        ReplaceIfNewer,					// Replace the existing file if the source is newer than the target
        ReplaceIfLarger,				// Replace the existing file if the source is larger than the target
        IgnoreIfExists,					// Ignore copying the node if the target exists
        RenameIfExists,					// Rename the incoming file, and retain the existing file
        BackupExisting,					// Rename the existing file, and copy the incoming file
        RenameFromUser,					// Rename the incoming file as the user wants
        SignalError 					// Treat the node as erroneous and signal the user (no-clobber)
    };

    /**
     * IOProcess is a struct that contains all the necessary details for showing a progress dialog.
     *
     *   - sources                 The files/directories which need to be copied
     *   - targets                 The directory to where the sources will be copied/moved
     *   - totalBytes              Total size of all sources
     *   - totalBytesCopied        Volume of IO that has taken place (bytes copied to the target)
     *   - fileBytes               The size of the file currently being processed
     *   - fileBytesCopied         Volume of the file already copied (bytes of the file copied)
     *   - progressText            Text to be shown by the progress widgets
     *   - type                    Type of process (See NBProcess::Type)
     *   - state                   Type of process (See NBProcess::State)
     *
     * Note: Paths for all the sources and the targets should be absolute or they will be treated as relative to current path
     * Example:
     *   If we want to copy all the files living in /usr/share/, then
     *       sources will be /usr/share/alsa, /usr/share/applications, /usr/share/aspell, ..., etc.
     *
     */
    typedef struct IOProcess_t {

        /** The source files and directories */
        QStringList sources;

        /** The target directories */
        QStringList targets;

        /** Total bytes to be copied */
        qint64 totalBytes = 1;

        /** Total bytes already copied */
        qint64 totalBytesCopied = 0;

        /** Current file name */
        QString currentFile;

        /** Current file size */
        qint64 fileBytes = 1;

        /** Current file bytes already copied */
        qint64 fileBytesCopied = 0;

        /** When did this process start */
        QDateTime startTime = QDateTime::currentDateTime();

        /** Text to be displayed with the progress bar */
        QString progressText = "Starting...";

        /** Type: Copy, Move, Delete, Trash */
        IOProcessType type = Copy;

        /** State: Starting, Started, Paused, etc.. */
        IOProcessState state = NotStarted;

        /** Collate
         * Finish copying to one target before going to the next
         * Collating is not a good idea when copying multiple files
         * A single file has to be read each time separately for
         * each target. By default collate is set to false.
         */
        bool collate = false;
    } IOProcess;
};
