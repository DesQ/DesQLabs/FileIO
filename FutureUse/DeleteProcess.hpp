/*
	*
	* NBDeleteProcess.hpp - NBDeleteProcess.cpp header
	*
*/

#pragma once

#include "Global.hpp"
#include "AbstractIOProcess.hpp"

namespace DesQ {
	class DeleteProcess;
};

class DesQ::DeleteProcess : public DesQ::AbstractIOProcess {
	Q_OBJECT;

	public:
		DeleteProcess( DesQ::IOProcess *process );

		// Can we undo this operation?
		bool canUndo() {

			return ( mProcess->type == DesQ::IOProcessType::Trash );
		};

		// Perform the undo
		void undo();

		// Nodes
		QStringList nodes() {

			return sourceList;
		};

	protected:
		void run();

	private:
		/* Delete the node (and its contents) forever */
		void deleteNode( QString path );

		/* Send the node (and its contents) to trash */
		void trashNode( QString path );

		/* Send the node (and its contents) to trash */
		void restoreNode( QString path );

		/* All the not directory nodes in the sources will be listed here */
		QStringList sourceList;

		QStringList errorNodes;

		bool mCanceled;
		bool mPaused;

		bool mUndo;

		NBProcess::Process *mProgress;
};
