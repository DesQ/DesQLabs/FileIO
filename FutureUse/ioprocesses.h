/*
    *
    * This file is a part of Libcprime.
    * Library for saving activites and bookmarks, share file and more.
	* Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/


#pragma once

#include <QThread>
#include <QMessageBox>
#include <QBasicTimer>

#include "cenums.h"

#include "libcprime_global.h"


class QProgressBar;
class QPushButton;
class IODialog;

class LIBCPRIMESHARED_EXPORT CIOProcess : public QThread {

    Q_OBJECT

public:
    CIOProcess( QStringList sources, CPrime::IOProcess *process, QWidget *parent );

    // The list of nodes which could not be copied/moved/archived
    QStringList errors();

    /* We need this because we cannot create dialogs from within the thread */
    QMessageBox::StandardButton resolution = QMessageBox::NoButton;

public Q_SLOTS:
    // Cancel the IO Operation
    void cancel();

    // Pause/Resume the IO Operation
    void pause();

    // Resume the paused IO
    void resume();

    // Nodes
    QStringList nodes() {
        return sourceList;
    }

protected:
    void run();

private:
    // Things to be done before IO begins like computing sizes
    bool preIO();

    // List the directory contents, and get the size
    void processDirectory( QString );

    // Copy a file
    void copyFile( QString );

    /* Get new filename */
    QString newFileName( QString );

    /* This will store the 'sources' list passed to the process  */
    QStringList origSources;

    /* All the not directory nodes in the sources will be listed here */
    QStringList sourceList;

    /* List of files which should not be deleted */
    QStringList keepList;

    QStringList errorNodes;

    bool mCanceled;
    bool mPaused;

    short int mResolveConflict = 0;

    CPrime::IOProcess *mProcess;
    IODialog *mParent;

Q_SIGNALS:
    /* Signals completion, with error list as the positional argument */
    void completed( QStringList );

    /* Signals cancelation, with errors encountered so far as the positional argument */
    void canceled( QStringList );

    /* Signals completion, with error list as the positional argument */
    void resolveConflict( QString, QString );

    /* Error: No write permission */
    void noWriteAccess();

    /* Error: Target does not have enough space */
    void noSpace();
};

class LIBCPRIMESHARED_EXPORT ConflictDialog : public QMessageBox {

    Q_OBJECT

public:
	static QMessageBox::StandardButton resolveConflict(const QString &file, QWidget *parent );

};

class LIBCPRIMESHARED_EXPORT IODialog : public QDialog {

    Q_OBJECT

public:
	explicit IODialog(QStringList source, CPrime::IOProcess *process );
	static void copy(QString &source, const QString &target );

private:
    QBasicTimer timer;

    QProgressBar *pBar;
    QPushButton *pauseBtn;

    CPrime::IOProcess *mProcess;
    CIOProcess *ioproc;

public Q_SLOTS:
    void show();

private Q_SLOTS:
    void togglePause();
    void cancelIO();

    void resolveConflict( QString, QString );

protected:
    void timerEvent( QTimerEvent * );
    void closeEvent( QCloseEvent * );
};
