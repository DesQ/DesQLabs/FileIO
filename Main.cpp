/*
	*
	* Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project (https://gitlab.com/desq/)
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* at your option, any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

// Local Headers
#include "Global.hpp"
#include "CopyProcess.hpp"
#include "IOProcess.hpp"

#include <desq/DesQUtils.hpp>
#include <desq/DesQCoreApplication.hpp>

DesQSettings* ioSett;

int main( int argc, char **argv ) {

	qputenv( "QT_WAYLAND_USE_BYPASSWINDOWMANAGERHINT", QByteArrayLiteral( "1" ) );
	DesQCoreApplication app( "DesQLock", argc, argv );

	app.setOrganizationName( "DesQ" );
	app.setApplicationName( "DesQ IO" );
	app.setApplicationVersion( VERSION_TEXT );

	ioSett = new DesQSettings( "IO" );

	DesQ::IOProcess *process = new DesQ::IOProcess();

	process->sources = QStringList(
		{
			"/path/to/source1",
			"/another/path/to/source2",
			"/a/different/path/to/source3",
		}
	);

	process->targets = QStringList(
		{
			"/path/to/external/drive/",
			"/path/to/some/writable/partition",
			"/path/to/home/folder",
			"/path/to/fome/folder/in/home/directory/",
		}
	);

	DesQ::CopyProcess *proc = new DesQ::CopyProcess( process );
	proc->start();

	proc->wait();

	return 0;
};
