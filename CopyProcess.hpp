/*
	*
	* Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project (https://gitlab.com/desq/)
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* at your option, any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#pragma once

#include "Global.hpp"
#include "AbstractIOProcess.hpp"

namespace DesQ {
	class CopyProcess;
	class MoveProcess;
}

class DesQ::CopyProcess : public DesQ::AbstractIOProcess {
	Q_OBJECT;

	public:
		CopyProcess( DesQ::IOProcess *process );

		/** We can always undo a copy - delete the nodes from the target */
		virtual bool canUndo() {

			return true;
		};

		// Perform the undo
		virtual void undo();

		// List of sources nodes
		QMap<QString, QStringList> nodes() {

			return sourceMap;
		};

		/** Resolve the conflicts when the target exists */
		virtual void resolveConflict( QString source, QString target, int resolution );

	protected:
		void run();

	private:
		// Things to be done before IO begins like computing sizes
		bool preprocess();

		// List the directory contents, and get the size
		void processDirectory( QString, QString );

		// Copy one file to multiple targets
		bool copyFile( QString, QStringList );

		/* Get new filename */
		QString newFileName( QString );

		/* Get backup filename: tackon .bak-yyyyMMddThhmmss */
		QString backupFileName( QString );

		void waitResolution();

		/* All the non directory nodes in the sources will be listed here */
		QMap<QString, QStringList> sourceMap;
		QStringList mTargets;

		typedef struct conflicted_t {
			QString target;
			int strategy;
		} ConflictedIO;

		QHash<QString, ConflictedIO> conflicts;
		QHash<QString, ConflictedIO> resolved;
};
