/*
	*
	* Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project (https://gitlab.com/desq/)
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* at your option, any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#pragma once

#include "Global.hpp"
#include "IOProcess.hpp"

namespace DesQ {
	class AbstractIOProcess;
	typedef QMap<QString, int> IOErrors;
}

class DesQ::AbstractIOProcess : public QThread {
	Q_OBJECT;

	public:
		// The list of nodes which could not be copied/moved/archived
		virtual DesQ::IOErrors errors() {

			return mErrorMap;
		};

		// Can we undo this operation?
		virtual bool canUndo() = 0;

		// Perform the undo
		virtual void undo() = 0;

		// List of sources
		virtual QMap<QString, QStringList> nodes() = 0;

		/** SLOTS */

		/** Resolve the conflict
		 * ReplaceExisting	- source and target same as conflict(...)
		 * ReplaceIfNewer	- source and target same as conflict(...)
		 * ReplaceIfLarger	- source and target same as conflict(...)
		 * IgnoreExisting	- source and target same as conflict(...)
		 * BackupExisting	- source and target same as conflict(...), but the existing file should be renamed
		 * RenameIfExists	- source is same as conflict(...), target will be used to generate a new name
		 * RenameFromUser	- source is same as conflict(...), target will have the new name (user-selected)
		 * SignalError		- source and target same as conflict(...)
		 */
		virtual void resolveConflict( QString source, QString target, int resolution ) = 0;

		// Cancel the IO Operation
		virtual void cancel() {

			mProcess->state = DesQ::IOProcessState::Canceled;
			mCanceled = true;
			qApp->processEvents();
		};

		// Pause the IO Operation
		virtual void pause() {

			mProcess->state = DesQ::IOProcessState::Paused;
			mPaused = true;
			qApp->processEvents();
		};

		// Resume the paused IO
		virtual void resume() {

			mProcess->state = DesQ::IOProcessState::Started;
			mPaused = false;
			qApp->processEvents();
		};

	protected:
		/* Force the subclass to implement the function run */
		virtual void run() = 0;

		/* The process object */
		DesQ::IOProcess *mProcess;

		/* Node -> Error Map */
		DesQ::IOErrors mErrorMap;

		/* Pause the process */
		bool mPaused = false;

		/* Cancel the process */
		bool mCanceled = false;

	Q_SIGNALS:
		/* Signals completion */
		void completed();

		/* Signals cancelation */
		void canceled();

		/* Errors; bool represents if the target has the error */
		void error( QString, int, bool );

		/* Intimate the user about conflicts between files */
		void conflict( QString, QString );

		/* Error: No read permission */
		void noReadAccess( QString );

		/* Error: No write permission */
		void noWriteAccess( QString );
};
