/**
	*
	* Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project (https://gitlab.com/desq/)
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* at your option, any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#include <desq/DesQUtils.hpp>

#include "CopyProcess.hpp"
#include <errno.h>

/** 128 KiB is the perfect size */
const long int BUFSIZE = 128 * 1024;

DesQ::CopyProcess::CopyProcess( DesQ::IOProcess *process ) : DesQ::AbstractIOProcess() {

	sourceMap.clear();

	mProcess = process;

	/** Initialize the sizes to zero */
	mProcess->totalBytes = 1;
	mProcess->totalBytesCopied = 0;
	mProcess->fileBytes = 1;
	mProcess->fileBytesCopied = 0;

	/** Initialize the state to DesQ::IOProcessState::NotStarted */
	mProcess->state = DesQ::IOProcessState::NotStarted;
};

void DesQ::CopyProcess::undo() {

};

void DesQ::CopyProcess::resolveConflict( QString source, QString target, int resolution ) {

	ConflictedIO resolve = { target, resolution };
	resolved[ source ] = resolve;

	conflicts.remove( source );
};

bool DesQ::CopyProcess::preprocess() {

	/** Update the state */
	mProcess->state = DesQ::IOProcessState::Starting;

	/** Check if the sources are readable, and process the directories */
	for( QString src: mProcess->sources ) {

		QString srcDir( DesQ::Utils::dirName( src ) );

		struct stat *st = ( struct stat* )malloc( sizeof( struct stat ) );

		if ( not sourceMap.contains( srcDir ) )
			sourceMap[ srcDir ] = QStringList();

		if ( lstat( src.toLocal8Bit().data(), st ) == -1 ) {
			emit error( src, errno, false );
			qWarning() << "[Error]:" << strerror( errno );

			continue;
		}

		if ( not DesQ::Utils::isReadable( src ) ) {
			emit error( src, EACCES, false );
			qWarning() << "[Error]:" << strerror( EACCES );

			continue;
		}

		switch( st->st_mode & S_IFMT ) {
			case S_IFDIR: {

				/** Recurse into that folder */
				processDirectory( src, srcDir );

				/** Break out of switch */
				break;
			}

			case S_IFREG: {

				/** Get the size of the current file */
				mProcess->totalBytes += st->st_size;

				/** Add this to the source file list */
				sourceMap[ srcDir ] << src;

				/** Break out of switch */
				break;
			}

			case S_IFLNK:
			case S_IFBLK:
			case S_IFCHR:
			case S_IFIFO: {

				/** Add this to the source file list */
				sourceMap[ srcDir ] << src;

				/** No changes in the size */

				/** Break out of switch */
				break;
			}

			case S_IFSOCK: {

				break;
			}
		}
	}

	/** Check if the targets are writable */
	QList<QStorageInfo> devs;
	QList<int> devCount;
	QList<QStringList> devTgts;
	for( QString tgt: mProcess->targets ) {
		if ( DesQ::Utils::mkpath( tgt ) ) {
			emit error( tgt, errno, true );
			mErrorMap[ tgt ] = errno;

			qWarning() << "[Error]:" << strerror( errno );
			continue;
		}

		QStorageInfo sInfo( tgt );
		if ( not sInfo.isValid() ) {
			emit error( tgt, EEXIST, true );
			mErrorMap[ tgt ] = EEXIST;

			qWarning() << "[Error]:" << strerror( EEXIST );
			continue;
		}

		if ( not devs.contains( sInfo ) ) {
			devs << sInfo;
			devCount << 0;
			devTgts << QStringList();
		}

		/** If the target is wriable */
		if ( DesQ::Utils::isWritable( tgt ) ) {
			/** Increase the count for the target drive */
			devCount[ devs.indexOf( sInfo ) ]++;
			devTgts[ devs.indexOf( sInfo ) ] << tgt;
		}
	}

	for( int i = 0; i < devs.count(); i++ ) {
		QStorageInfo sInfo( devs[ i ] );
		if ( sInfo.bytesAvailable() >= devCount[ i ] * mProcess->totalBytes ) {
			for( QString tgt: devTgts[ i ] ) {
				tgt += ( tgt.endsWith( "/" ) ? "" : "/" );
				mTargets << tgt;
			}
		}

		else {
			for( QString tgt: devTgts[ i ] ) {
				mErrorMap[ tgt ] = ENOSPC;
				emit error( tgt, ENOSPC, false );

				qWarning() << "Error coping files to" << tgt;
				qWarning() << "[Error]:" << strerror( ENOSPC );
			}
		}
	}

	/** Ensure we have something to write */
	int srcCount = 0;
	for( QString src: sourceMap.keys() ) {
		if ( sourceMap[ src ].count() )
			srcCount++;
	}

	/** Nothing left to copy */
	if ( not srcCount ) {

		/** Don't proceed */
		return false;
	}

	/** Nowhere to write */
	if ( not mTargets.count() ) {

		/** Don't proceed */
		return false;
	}

	/** If we have come so far, something can be writen somewhere. */
	return true;
};

void DesQ::CopyProcess::processDirectory( QString path, QString src ) {

	DIR* d_fh;
	struct dirent* entry;

    while ( ( d_fh = opendir( path.toLocal8Bit().data() ) ) == nullptr ) {
		mErrorMap[ path ] = errno;
		emit error( path, errno, false );

		qWarning() << "[Error]:" << path << strerror( errno );

		return;
	}

	/** Add a '/' at the end of @path */
	if ( not path.endsWith( "/" ) )
		path += "/";

	/** Now, we can read what is inside this directory */
    while( ( entry = readdir( d_fh ) ) != nullptr ) {

		/** Don't descend up the tree or include the current directory */
		if ( strcmp( entry->d_name, ".." ) != 0 && strcmp( entry->d_name, "." ) != 0 ) {

			/** Stat the entry to get the type and mode */
			struct stat st;
			if ( lstat( ( path + entry->d_name ).toLocal8Bit().data(), &st ) == -1 ) {
				continue;
			}

			switch( st.st_mode & S_IFMT ) {
				case S_IFDIR: {

					/** Recurse into that folder */
					processDirectory( path + entry->d_name, src );

					/** Break out of switch */
					break;
				}

				case S_IFREG: {
					/** Get the size of the current file */
					mProcess->totalBytes += DesQ::Utils::getSize( path + entry->d_name );

					/** Add this to the source file list */
					sourceMap[ src ] << path + entry->d_name;

					/** Break out of switch */
					break;
				}

				case S_IFLNK:
				case S_IFBLK:
				case S_IFCHR:
				case S_IFIFO: {
					/** Add this to the source file list */
					sourceMap[ src ] << path + entry->d_name;

					/** No changes in the size */

					/** Break out of switch */
					break;
				}

				case S_IFSOCK: {
					break;
				}
			}
		}
	}

	closedir( d_fh );
};

bool DesQ::CopyProcess::copyFile( QString srcFile, QStringList tgtFiles ) {

	QThread::setPriority( QThread::LowestPriority );

	char buffer[ BUFSIZE ] = { 0 };

	qint64 readBytes = 0;
	qint64 inBytes = 0;
	qint64 bytesWritten = 0;

	struct stat iStat;
	stat( srcFile.toLocal8Bit().data(), &iStat );

	/** Open the input file descriptor for reading */
	FILE *iFile = fopen( srcFile.toLocal8Bit().data(), "rb" );

	/** Open the output file descriptor for reading */
	QList<FILE *> oFileList;
	for( QString tgtFile: tgtFiles ) {
		FILE *oFile = fopen( tgtFile.toLocal8Bit().data(), "wb" );
		oFileList << oFile;
	}

	/** DesQ::IOProcess::fileBytes */
	mProcess->fileBytes = iStat.st_size;

	/** DesQ::IOProcess::fileBytesCopied */
	mProcess->fileBytesCopied = 0;

	/** While we read positive chunks of data we write it */
	while ( ( iStat.st_size != mProcess->fileBytesCopied ) and ( not feof( iFile ) ) ) {

		if ( mCanceled ) {
			fclose( iFile );
			for( FILE *oFile: oFileList )
				fclose( oFile );

			emit canceled();

			return false;
		}

		while ( mPaused ) {
			if ( mCanceled ) {
				fclose( iFile );
				for( FILE *oFile: oFileList )
					fclose( oFile );

				emit canceled();

				return false;
			}

			usleep( 100 );
			qApp->processEvents();
		}

		/** Prepare for next read: smaller of (data remaining, BUFSIZE) */
		readBytes = ( iStat.st_size - mProcess->fileBytesCopied < BUFSIZE ? iStat.st_size - mProcess->fileBytesCopied : BUFSIZE );

		/** Read the requisite number of bytes */
		inBytes = fread( buffer, sizeof( char ), readBytes, iFile );

		if ( inBytes != readBytes ) {
			emit error( srcFile, errno, false );
			mErrorMap[ srcFile ] = errno;

			qWarning() << "Error reading file:" << srcFile;
			qWarning() << "Expected" << readBytes << "got" << inBytes;
			qWarning() << "[Error]:" << strerror( errno );
		}

		/** We read non-zero number of bytes */
		if ( inBytes > 0 ) {

			/** Write those bytes */
			for( FILE *oFile: oFileList ) {
				bytesWritten = fwrite( buffer, sizeof( char ), inBytes, oFile );

				if ( bytesWritten != inBytes ) {
					mErrorMap[ srcFile ] = errno;
					emit error( tgtFiles.value( oFileList.indexOf( oFile ) ), errno, true );

					qWarning() << "Error writing to file:" << tgtFiles.value( oFileList.indexOf( oFile ) );
					qWarning() << "[Error]:" << strerror( errno );
				}
			}

			/** Update the info */
			mProcess->fileBytesCopied += bytesWritten;
			mProcess->totalBytesCopied += bytesWritten;
		}
	}

	fclose( iFile );
	for( FILE *oFile: oFileList )
		fclose( oFile );

	if ( mProcess->fileBytesCopied != iStat.st_size ) {
		mErrorMap[ srcFile ] = errno;
		return false;
	}

	mode_t srcMode = DesQ::Utils::getMode( srcFile );
	for( QString tgtFile: tgtFiles ) {
		if ( chmod( tgtFile.toLocal8Bit().data(), srcMode ) ) {
			mErrorMap[ tgtFile ] = errno;
			emit error( tgtFile, errno, true );

			qWarning() << "Error setting file mode" << tgtFile;
			qWarning() << "[Error]:" << strerror( errno );
		};
	}

	return true;
};

QString DesQ::CopyProcess::newFileName( QString fileName ) {

	int i = 0;
	QString newFile;

	QString dir = DesQ::Utils::dirName( fileName );
	QString file = DesQ::Utils::baseName( fileName );

	do {
		newFile = QString( "%1/Copy (%2) - %3" ).arg( dir ).arg( i ).arg( file );
		i++;
	} while( DesQ::Utils::exists( newFile ) );

	return newFile;
};

void DesQ::CopyProcess::run() {

	if ( mCanceled ) {
		emit canceled();

		quit();
		return;
	}

	while ( mPaused ) {
		if ( mCanceled ) {
			emit canceled();

			return;
		}

		usleep( 100 );
		qApp->processEvents();
	}

	/** First we process the sources */
	if ( not preprocess() ) {

		mProcess->state = DesQ::IOProcessState::Completed;
		emit completed();

		quit();
		return;
	}

	/** Actual IO Begins */
	for( QString src: sourceMap.keys() ) {

		if ( mCanceled ) {
			emit canceled();

			quit();
			return;
		}

		while ( mPaused ) {
			if ( mCanceled ) {
				emit canceled();

				return;
			}

			usleep( 100 );
			qApp->processEvents();
		}

		for( QString node: sourceMap[ src ] ) {
			QStringList tgtList;
			QString nodeBase( QString( node ).replace( src, "" ) );

			for( QString tgt: mTargets ) {
				QString tgtFile = tgt + nodeBase;

				/** Generate the file-system tree in the target folder */
				QStringList tokens = nodeBase.split( "/" );
				tokens.removeLast();			// The last token will always be a file

				QString tgtPath = tgt;
				QString srcPath = src;
				for( QString token: tokens ) {
					tgtPath += token + "/";
					srcPath += token + "/";

					if ( mkdir( tgtPath.toLocal8Bit().data(), DesQ::Utils::getMode( srcPath ) ) ) {
						if ( errno != EEXIST ) {
							emit error( node, errno, false );
							qWarning() << "Error creating directory tree on target.";
							qWarning() << "[Error]:" << strerror( errno );
						}
					}
				}

				if ( DesQ::Utils::exists( tgtFile ) ) {
					ConflictedIO conflicted = { tgtFile, 0 };
					conflicts[ node ] = conflicted;

					emit conflict( node, tgtFile );
				}

				else
					tgtList << tgtFile;
			}

			/**
			 * No target files => They exist or some other error.
			 * Proceed to next source file
			 */
			if ( not tgtList.count() )
				continue;

			struct stat iStat;
			lstat( node.toLocal8Bit().data(), &iStat );

			switch( iStat.st_mode & S_IFMT ) {
				case S_IFREG: {

					copyFile( node.toLocal8Bit().data(), tgtList );
					break;
				}

				case S_IFLNK: {
					char linkTgt[ 4096 ] = { 0 };
					readlink( node.toLocal8Bit().data(), linkTgt, 4096 );
					for( QString tgtFile: tgtList ) {
						if ( symlink( linkTgt, tgtFile.toLocal8Bit().data() ) ) {
							emit error( tgtFile, errno, true );
							mErrorMap[ tgtFile ] = errno;

							qWarning() << "Error creating symlink:" << tgtFile << linkTgt;
							qWarning() << "[Error]:" << strerror( errno );
						}
					}

					break;
				}

				case S_IFBLK:
				case S_IFCHR: {
					for( QString tgtFile: tgtList ) {
						if ( mknod( tgtFile.toLocal8Bit().data(), iStat.st_mode, iStat.st_dev ) ) {
							emit error( tgtFile, errno, true );
							mErrorMap[ tgtFile ] = errno;

							qWarning() << "Error creating node:" << tgtFile;
							qWarning() << "[Error]:" << strerror( errno );
						}
					}

					break;
				}

				case S_IFIFO: {
					mode_t srcMode = DesQ::Utils::getMode( node );
					for( QString tgtFile: tgtList ) {
						if ( mkfifo( tgtFile.toLocal8Bit().data(), srcMode ) ) {
							emit error( tgtFile, errno, true );
							mErrorMap[ tgtFile ] = errno;

							qWarning() << "Error creating node:" << tgtFile;
							qWarning() << "[Error]:" << strerror( errno );
						}
					}

					break;
				}

				case S_IFSOCK: {
					break;
				}

				default: {
					/** We should never have come here */
					break;
				}
			}
		}
	}

	// waitResolution();
};

void DesQ::CopyProcess::waitResolution() {

	while( conflicts.count() or resolved.count() ) {
		if ( resolved.count() ) {

			for( QString src: resolved.keys() ) {

				ConflictedIO solution = resolved[ src ];
				switch( solution.strategy ) {
					case ReplaceExisting: {

						/** Remove the existing file */
						unlink( solution.target.toLocal8Bit().data() );

						/** Copy the file without delay */
						copyFile( src, { solution.target } );
						break;
					}

					case ReplaceIfNewer: {

						/** Stat to check the modification times */
						struct stat sStat;
						struct stat tStat;

						lstat( src.toLocal8Bit().data(), &sStat );
						lstat( solution.target.toLocal8Bit().data(), &tStat );

						/** If the source mtime is > tgt mtime, copy it */
						if ( sStat.st_mtim.tv_sec > tStat.st_mtim.tv_sec ) {

							/** Remove the existing file */
							unlink( solution.target.toLocal8Bit().data() );

							/** Copy the file without delay */
							copyFile( src, { solution.target } );
						}

						break;
					}

					case ReplaceIfLarger: {

						/** Stat to check the sizes */
						struct stat sStat;
						struct stat tStat;

						lstat( src.toLocal8Bit().data(), &sStat );
						lstat( solution.target.toLocal8Bit().data(), &tStat );

						if ( sStat.st_size > tStat.st_size ) {
							/** Remove the existing file */
							unlink( solution.target.toLocal8Bit().data() );

							/** Copy the file without delay */
							copyFile( src, { solution.target } );
						}

						break;
					}

					case IgnoreIfExists: {

						/** We don't have to do anything at all */

						break;
					}

					case RenameIfExists: {

						/** Get an alternate file name */
						QString newTgtFile = newFileName( solution.target );

						/** Copy the file without delay */
						copyFile( src, { solution.target } );

						break;
					}

					case BackupExisting: {

						/** Rename the existing file */
						QString bak = QDateTime::currentDateTime().toString( "yyyyMMddThhmmss" );
						rename( solution.target.toLocal8Bit().data(), ( solution.target + bak ).toLocal8Bit().data() );

						/** Copy the file without delay */
						copyFile( src, { solution.target } );

						break;
					}

					case RenameFromUser: {

						/** Copy the file without delay; soultion has the new name */
						copyFile( src, { solution.target } );

						break;
					}

					case SignalError: {

						error( src, EEXIST, false );
					}
				}

				/** Remove the solution */
				resolved.remove( src );
			}
		}

		else {
			usleep( 100 );
			qApp->processEvents();
		}
	}
};
